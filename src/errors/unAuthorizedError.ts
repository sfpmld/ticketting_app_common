import { CustomError } from './customError';

export class UnAuthorizedError extends CustomError {
  statusCode = 401;
  message = 'Not authorized';

  constructor() {
    super('Not authorized');
    Object.setPrototypeOf(this, UnAuthorizedError.prototype);
  }

  serializeErrors() {
    return [
      {
        message: this.message,
      },
    ];
  }
}


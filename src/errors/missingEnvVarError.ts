import { CustomError } from './customError';

export class MissingEnvVarError extends CustomError {
  statusCode = 500;

  constructor(varName: string) {
    super(`env variable "${varName}" is missing`);
    Object.setPrototypeOf(this, MissingEnvVarError.prototype);
  }

  serializeErrors() {
    return [
      {
        message: this.message,
      },
    ];
  }
}


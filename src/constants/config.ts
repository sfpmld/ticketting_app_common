import { getFromEnv } from "../helpers";
import { Secret } from "jsonwebtoken";

export const NODE_ENV = getFromEnv("NODE_ENV");
export const JWT_SALT = getFromEnv("JWT_SALT") as Secret;

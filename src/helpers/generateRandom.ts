import { randomBytes } from 'crypto';

export const generateRandom = () => randomBytes(4).toString('hex');

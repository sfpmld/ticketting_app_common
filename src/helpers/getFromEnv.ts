import { MissingEnvVarError } from '../errors';

export const getFromEnv = (varName: string): string => {
  const envVar = process.env[`${varName}`];
  if (!envVar) throw new MissingEnvVarError(varName);
  return envVar;
};

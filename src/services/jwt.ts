import jwt, { Secret } from 'jsonwebtoken';
import { JWT_SALT } from '../constants';

export interface Payload {
  id: string;
  email: string;
}

export class Jwt {
  static generateToken = (payload: Payload) => {
    return jwt.sign(payload, JWT_SALT);
  };

  static decodeToken = (token: string, salt: Secret) => {
    let decodedToken;
    try {
      decodedToken = jwt.verify(token, salt);
    } catch (err) {}

    return decodedToken;
  };
}

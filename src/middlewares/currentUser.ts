import { NextFunction, Request, Response } from 'express';

import { Jwt } from '../services';
import { JWT_SALT } from '../constants';

// Enhance the express Request to add business payload
declare global {
  namespace Express {
    export interface Request {
      currentUser?: UserPayloadAttrs;
    }
  }
}

export interface UserPayloadAttrs {
  id: string;
  email: string;
}

export const currentUser = (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  if (!req.session?.token) return next();

  const payload = Jwt.decodeToken(
    req.session.token,
    JWT_SALT
  ) as UserPayloadAttrs;
  req.currentUser = payload;

  next();
};

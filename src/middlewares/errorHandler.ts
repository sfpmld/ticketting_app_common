import { Request, Response, NextFunction } from 'express';
import { CustomError, SerializedCustomError } from '../errors';

export const errorHandler = (
  err: Error,
  _req: Request,
  res: Response,
  _next: NextFunction
) => {
  const defaultMessage: SerializedCustomError = {
    message: 'Sorry, something went wrong',
  };

  if (err instanceof CustomError) {
    return res.status(err.statusCode).send({ errors: err.serializeErrors() });
  } else {
    console.log(err);
    res.status(500).send({
      errors: [defaultMessage],
    });
  }
};

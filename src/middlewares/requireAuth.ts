import { Request, Response, NextFunction } from 'express';
import { UnAuthorizedError } from '../errors';

export const requireAuth = async (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  if (!req.currentUser) {
    throw new UnAuthorizedError();
  }
  next();
};

export enum OrderStatus {
  // When the order has been created, but the ticket it is trying to order has
  // not been reserved
  Created = 'created',
  // When ticket the order is trying to reserve has already been reserved, or
  // When the user has cancelled the order or
  // When the order expires
  Cancelled = 'cancelled',
  // When the order has successfully reserved the ticket
  AwaitingPayment = 'awaiting:payment',
  // When the order has reserved the ticket and the user has provided payment
  // successfully
  Complete = 'complete',
}
